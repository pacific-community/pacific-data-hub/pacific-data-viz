# Static website for Pacific Dataviz Challenge
## Development

### Source files during development

This website uses [node-static-i18n](https://www.npmjs.com/package/static-i18n) to handle translation.

Source files are located in the folder `src`.

To start development:

* In `/src/locales/en.json` and `/src/locales/fr.json` 
  Change value of `revised` from `YYYY-MM-DD` to `source`

* Youtube channel key:  
  In `js/scripts.source.js`, make sure you do not use the "PacificDataViz" API key.  
  Use your own.

* If needed, make your changes in:
  * HTML: `/src/index.html` (and bluid pages, see below)
  * CSS: `/css/styles.source.css`
  * JS:  `/js/scripts.source.js`

### Static-i18n building

To build the translated pages, run
```bash
npm install -g static-i18n
static-i18n -l en -i en -i fr -o . src/
```
`./index.html` and `./fr/index.html` will be created.
 

## Deployment

Before committing changes:

* Update "revised" meta tag before any content update (format = 'YYYY-MM-DD')
* If any changes been made to Minify your javascript and CSS
  * JS: https://codebeautify.org/minify-js
  * CSS: https://codebeautify.org/css-beautify-minify
* Both JS and CSS filenames should match the revised tag (YYYY-MM-DD)
  * JS: `/js/scripts.YYYY-MM-DD.js`
  * CSS: `/css/styles.YYYY-MM-DD.css`
* Rebuild static pages
* Make sure the Youtube API key is the PacificDataViz one
* Test page ONCE and only ONCE (consumes Google API units)
* Commit and push your branch
* Create a Pull request on gitlab