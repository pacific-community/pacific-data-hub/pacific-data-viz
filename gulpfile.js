const gulp = require('gulp');
const concat = require('gulp-concat');
const cleanCss = require('gulp-clean-css');
const minify = require('gulp-minify');
const mustache = require('gulp-mustache');
const connect = require('gulp-connect');
const uniqid = require('uniqid');
const fs = require('fs')

let uid = ''

// Concat and minify CSS files
gulp.task('build-css', () => {
    let outputName = 'app.css';
    if (uid !== '') {
        outputName = `app-${uid}.css`;
    }
    return gulp.src('src/*.css')
    .pipe(concat(outputName))
    .pipe(cleanCss())
    .pipe(gulp.dest('dist/css'));
});

gulp.task('build-js', () => {
    let outputName = 'app.js';
    if (uid !== '') {
        outputName = `app-${uid}.js`;
    }
    return gulp.src('src/*.js')
    .pipe(concat(outputName))
    .pipe(minify())
    .pipe(gulp.dest('dist/js'));
});
gulp.task("session-start", (cb) => {
    return gulp.series('build-css')(cb);
});

gulp.task("assets", () => {
    return gulp.src('assets/**/*')
    .pipe(gulp.dest('./dist/assets'));
});

gulp.task("html", () => {
    let data = JSON.parse(fs.readFileSync('src/locales/en.json'));
    if (uid !== '') {
        data = {...data, uid: `-${uid}`};
    }
    let entries = JSON.parse(fs.readFileSync('src/entries.json'));
    if (entries.length > 0) {
        entries.map(entry => {
            if (entry['category'] == 'Dataviz Interactive'){
                entry['category'] = 'Interactive dataviz';
            } else {
                entry['category'] = 'Static dataviz'
            }
        })
        data = {...data, entries: entries};
    }
    return gulp.src('src/*.html')
    .pipe(mustache(data))
    .pipe(gulp.dest('./dist'));
});
gulp.task("html-fr", () => {
    let data = JSON.parse(fs.readFileSync('src/locales/fr.json'));
    if (uid !== '') {
        data = {...data, uid: `-${uid}`};
    }
    let entries = JSON.parse(fs.readFileSync('src/entries.json'));
    if (entries.length > 0) {
        data = {...data, entries: entries};
    }
    return gulp.src('src/*.html')
    .pipe(mustache(data))
    .pipe(gulp.dest('./dist/fr'));
});

gulp.task('dev', () => {
    gulp.watch('assets/**/*',{ ignoreInitial: false }, gulp.series('assets'));
    gulp.watch('src/*.html',{ ignoreInitial: false }, gulp.series('html', 'html-fr'));
    gulp.watch('src/locales/*.json',{ ignoreInitial: false }, gulp.series('html', 'html-fr'));
    gulp.watch('src/*.css',{ ignoreInitial: false }, gulp.series('build-css'));
    gulp.watch('src/*.js',{ ignoreInitial: false }, gulp.series('build-js'));
    connect.server({
        port: 3000,
        root: 'dist',
    });
});

gulp.task('inituid', (cb) => {
    uid = uniqid();
    cb();
});

gulp.task('build', gulp.series('inituid', 'build-css', 'build-js', 'html', 'html-fr', 'assets'));