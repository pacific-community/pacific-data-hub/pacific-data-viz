/*!
 * Start Bootstrap - Agency v7.0.11 (https://startbootstrap.com/theme/agency)
 * Copyright 2013-2022 Start Bootstrap
 * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-agency/blob/master/LICENSE)
 */

window.addEventListener("DOMContentLoaded", (event) => {
  // Navbar shrink function
  var navbarShrink = function () {
    const navbarCollapsible = document.body.querySelector("#mainNav");
    if (!navbarCollapsible) {
      return;
    }
    if (window.scrollY === 0) {
      navbarCollapsible.classList.remove("navbar-shrink");
    } else {
      navbarCollapsible.classList.add("navbar-shrink");
    }
  };

  // Shrink the navbar
  navbarShrink();

  // Shrink the navbar when page is scrolled
  document.addEventListener("scroll", navbarShrink);

  // Activate Bootstrap scrollspy on the main nav element
  const mainNav = document.body.querySelector("#mainNav");
  if (mainNav) {
    new bootstrap.ScrollSpy(document.body, {
      target: "#mainNav",
      offset: 74,
    });
  }

  // Collapse responsive navbar when toggler is visible
  const navbarToggler = document.body.querySelector(".navbar-toggler");
  const responsiveNavItems = [].slice.call(
    document.querySelectorAll("#navbarResponsive .nav-link")
  );
  responsiveNavItems.map(function (responsiveNavItem) {
    responsiveNavItem.addEventListener("click", () => {
      if (window.getComputedStyle(navbarToggler).display !== "none") {
        navbarToggler.click();
      }
    });
  });

  // set link active in the menu when changing page
  var current = location.pathname.split("/")[1];
  if (current === 'challenge-2023.html') {
    const challenge2023Link = document.body.querySelector("#challenge2023-link")
    if (challenge2023Link) {
      challenge2023Link.classList.add("active");
    }
  } else if (current === 'challenge-2022.html') {
    const challenge2022Link = document.body.querySelector("#challenge2022-link")
    if (challenge2022Link) {
      challenge2022Link.classList.add("active");
    }
  }

  if (document.getElementById("youtube")) {
    // Add a javascript widget to list latest videos from a youtube channel
    // https://www.youtube.com/channel/@PacificDatavizChallenge
    // https://developers.google.com/youtube/v3/docs/search/list
    var channelID = "UCNNF6Mw1tz4-9V5Cx9_-QlA";
    // var apiKey = "AIzaSyAvGYaZYUEdZUvYybjlFVL-R2OGtn1htQA"; // PcificDataViz
    var apiKey = "AIzaSyBxN8TlH2g3jwb5GiDFyQ268sQ7nqYvsVM"; // Stan SPC
    // var apiKey = "AIzaSyALqHIeHcg04zh1Fv48ggGEU4xIM2jpi1E"; // Stan Perso
    var reqURL = "https://www.googleapis.com/youtube/v3/search?order=date&part=snippet"
      + "&channelId=" + channelID
      + "&maxResults=9"
      + "&key=" +  apiKey;
    
    // fetch (GET) reqURL using native javascript
    // and loop through responses to build html
    fetch(reqURL)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        var link = "https://www.youtube.com/watch?v=";
        var entries = data.items;
        var output = '<div id="carouselVideos" class="carousel slide" data-bs-ride="false">';
        output += '<div class="carousel-inner">';
        var count = 0;
        entries.forEach((val, idx) => {
          if (val.id.kind == "youtube#video") {
            if (!count || count % 3 == 0) {
              if (count) {
                output += '</div></div>';
              }
              output += '<div class="carousel-item';
              if (!count) {
                output += ' active';
              }
              output += '"><div class="row';
              if (count) {
                output += ' d-none d-lg-flex';
              }
              output += '">';
            }
            output += '<div class="col-md-4 pb-4 pb-md-0">'
              + '<div class="card h-100">'
              + '<a href="'+link+val.id.videoId+'" target="_blank" class="video-icon">'
              + '<img src="'+val.snippet.thumbnails.medium.url+'" class="card-img-top" alt="'+val.snippet.title+'">'
              + '</a>'
              + '<div class="card-body">'
              + '<h5 class="card-title">'
              + val.snippet.title
              + '</h5>'
              + '<a href="'+link+val.id.videoId+'" target="_blank">Watch video</a>'
              +'</div></div></div>';
            count++;
          }
        });
        output += '</div></div>';
        output += "</div>"; // end carousel-inner

        // output += '<button class="carousel-control-prev" type="button" data-bs-target="#carouselVideos" data-bs-slide="prev">'
        //   + '<span class="carousel-control-prev-icon" aria-hidden="true"></span>'
        //   + '<span class="visually-hidden">Previous</span>'
        //   + '</button>'
        //   + '<button class="carousel-control-next" type="button" data-bs-target="#carouselVideos" data-bs-slide="next">'
        //   + '<span class="carousel-control-next-icon" aria-hidden="true"></span>'
        //   + '<span class="visually-hidden">Next</span>'
        //   + '</button>';

        output += '<div class="carousel-indicators">'
          +'<button type="button" data-bs-target="#carouselVideos" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>'
          +'<button type="button" data-bs-target="#carouselVideos" data-bs-slide-to="1" aria-label="Slide 2"></button>'
          +'</div>';

        output += "</div>"; // end carouselVideos
        var dest = document.getElementById("youtube");
        dest.innerHTML = output;
        dest.className = "mb-lg-5 pb-md-4";
        var myCarousel = document.querySelector('#carouselVideos');
        new bootstrap.Carousel(myCarousel, {
          interval: false,
          keyboard: false,
          ride: false,
          wrap: false
        });
      })
      .catch((err) => {
        document.getElementById("youtube").innerHTML = '';
      });
  }
});
